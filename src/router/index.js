import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/team',
    name: 'Teams',
    component: () => import('../components/Teams.vue')
  },
  {
    path: '/team/:teamId',
    name: 'TeamsDetail',
    component: () => import('../components/OneTeam.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
